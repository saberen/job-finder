# FOB FINDER 

# packages:
  ### 1.Django, version = 2.2.7
  ### 2.djangorestframework,  version = 3.10.3 
  ### 3.Pillow, version = 2.2.2

# APIs
## 1. ApplicantUser:
### A.create a new Applicant User:
route: /api/users/applicants/<br>
method: post<br>
permission: AllowAny<br>
header: None<br>
Feilds:
>{ <br>
"user"{<br>
"username" : "value",<br>
"password" : "value",<br>
"first_name" : "value",<br>
"last_name" : "value",<br>
"email" : "value"<br>
}<br>
"age":value,<br>
"gender":"M" or "F",<br>
skills:[&nbsp;list of {&nbsp;"name": "value"&nbsp;}&nbsp;]<br>
}
#### example request
```json
  {
	"user" : {
      "username" : "saber_ebrahimi",
      "password" : "Saber0028",
      "first_name" : "saber",
      "last_name" : "ebrahimi",
      "email" : "saber@eu.go"
	},
	"age" : 20,
    "gender": "M",
    "skills" :[{
    	"name" : "prg"
    }]
  }
```
### example respone
```json
{
    "pk": 49,
    "user": {
        "pk": 97,
        "username": "saber_ebrahimi",
        "password": "Saber0028",
        "first_name": "saber",
        "last_name": "ebrahimi",
        "email": "saber@eu.go"
    },
    "age": 20,
    "gender": "M",
    "resume": null,
    "skills": [
        {
            "name": "prg"
        }
    ],
    "token": "81434e43cfd7d8ae9781b1256cb90d0fe3244240"
}
```
you need to save token for futher usage
________________________________________________________
### B.change a Applicant User data:
route: /api/users/applicants/\<pk>/<br>
method: patch<br>
permission: IsApplicantOwner<br>
header: Applicant User Token<br>
Feilds:
>{ <br>
"age":value,<br>
"gender":"M" or "F",<br>
skills:[&nbsp;list of {&nbsp;"name": "value"&nbsp;}&nbsp;]<br>
}

these fields are not required 
________________________________________________________
### C.create a new Employer User:
route: /api/users/employer/<br>
method: post<br>
permission: AllowAny<br>
header: None<br>
Feilds:
>{ <br>
"user"{<br>
"username" : "value",<br>
"password" : "value",<br>
"first_name" : "value",<br>
"last_name" : "value",<br>
"email" : "value"<br>
}<br>
"company_name":value,<br>
"establish_year":"value,<br>
"address":"value,<br>
"telephone_number":"value,<br>
fields:[&nbsp;list of {&nbsp;"name": "value"&nbsp;}&nbsp;]<br>
}
#### example request
```json
 {
    "user": {
        "username": "saber-ebrahimi-",
        "password": "Sa028",
        "first_name": "saber",
        "last_name": "ebrahimi",
        "email": "saber@eu.go"
    },
    "company_name": "name",
    "establish_year": 1380,
    "address":"somewhere,",
    "telephone_number": "03432453004",
    "fields": [{"name":"prg"}]
}
```
### example respone
```json
{
    "pk": 25,
    "user": {
        "pk": 98,
        "username": "saber-ebrahimi-",
        "password": "Sa028",
        "first_name": "saber",
        "last_name": "ebrahimi",
        "email": "saber@eu.go"
    },
    "company_name": "name",
    "telephone_number": "03432453004",
    "establish_year": 1380,
    "address": "somewhere,",
    "fields": [
        {
            "name": "prg"
        }
    ],
    "token": "8d8d39f1c2cf299597ce049677983325a9d82d27"
}
```
you need to save token for futher usage
________________________________________________________
### D.change Employer User data:
route: /api/users/employer/\<pk><br>
method: patch<br>
permission: IsEmployerOwner<br>
header: employer user token<br>
Feilds:
>{ <br>
"company_name":value,<br>
"establish_year":"value,<br>
"address":"value,<br>
"telephone_number":"value,<br>
fields:[&nbsp;list of {&nbsp;"name": "value"&nbsp;}&nbsp;]<br>
}
________________________________________________________
### E.create a new job position:
route: /api/jobs/<br>
method: post<br>
permission: IsEmployer<br>
header: employer user token<br>
Feilds:
>{ <br>
"title": "value",<br>
"expire_date":"year-month-day",<br>
"salary":value,<br>
"monthly_hours":value,<br>
"applicants":[&nbsp;]<br>
"picture":"value",<br>
fields:[&nbsp;list of {&nbsp;"name": "value"&nbsp;}&nbsp;]<br>
}
#### example request
```json
 {
    "title": "back end",
    "expire_date":"2020-1-1",
    "salary":1200000,
    "monthly_hours":125,
    "applicants":[],
    "picture":null,
    "fields": [{"name":"prg"}]
}
```
### example respone
```json
{
    "pk": 61,
    "title": "back end",
    "expire_date": "2020-01-01",
    "picture": null,
    "fields": [
        {
            "name": "prg"
        }
    ],
    "salary": 1200000,
    "monthly_hours": 125,
    "owner": 23
}
```
________________________________________________________
### F.change job position data:
route: /api/jobs/\<pk><br>
method: patch<br>
permission: IsOwner<br>
header: employer user token<br>
Feilds:
>{ <br>
"title": "value",<br>
"expire_date":"year-month-day",<br>
"salary":value,<br>
"monthly_hours":value,<br>
"applicants":[&nbsp;]<br>
"picture":"value",<br>
fields:[&nbsp;list of {&nbsp;"name": "value"&nbsp;}&nbsp;]<br>
}
________________________________________________________
### G.create a new job position:
route: /api/applications/<br>
method: post<br>
permission: IsApplicant<br>
header: applicant user token<br>
Feilds:
>{ <br>
"job": job_pk,<br>
}
#### example request
```json
{
	"job": 62
}
```
### example respone
```json
{
    "pk": 21,
    "job": 62,
    "applicant": 47,
    "status": "W"
}
```
________________________________________________________
### H.cahnge status of an application:
route: /api/applications/<br>
method: post<br>
permission: IsJobOwner<br>
header: ownew employer user token<br>
Feilds:
>{ <br>
"status": "A" or "R"<br>
}
#### example request
```json
{
	"status": "A"
}
```
### example respone
```json
{
    "pk": 23,
    "job": 62,
    "applicant": 50,
    "status": "A"
}
```
_______________________________________________________________
### I.show a applicant related jobs:
route: /api/job/show/<br>
method: get<br>
permission: IsApplicant<br>
header: applicant user token<br>
### example respone
```json
    ["back end ": {
        "id": 62,
        "title": "back end dev",
        "expire_date": "2020-01-01",
        "salary": 1200000,
        "monthly_hours": 125,
        "owner": 25
      },...
    ]
  ```
  ____________________________________________________________
  ### J.search in jobs:
route: /api/jobs/search/<br>
method: post<br>
permission: IsAuthenticated<br>
header: user token<br>
Feilds:
>{ <br>
"tilte": "value"<br>
}
_________________________________________________________
  ### K.filter jobs:
route: /api/jobs/search/<br>
method: post<br>
permission: IsAuthenticated<br>
header: user token<br>
Feilds:
>{ <br>
"min_salary": value<br>
"max_salary":value<br>
}
__________________________________________________________
### L.all job positions:
route: /api/jobs/<br>
method: get<br>
permission: - <br>
header: - <br>