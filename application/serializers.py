from rest_framework import serializers

from job.models import JobModel

from user.models import ApplicantUser

from .models import ApplicationModel

class ApplicationModelSerializer(serializers.ModelSerializer):
  status = serializers.CharField(required=False)
  job = serializers.PrimaryKeyRelatedField(read_only=True)
  applicant = serializers.PrimaryKeyRelatedField(read_only=True)
  class Meta:
    model = ApplicationModel
    fields = [
      'pk',
      'job',
      'applicant',
      'status'
    ] 

  def create(self, validated_data):
    print("application serializer")
    print(validated_data, self.initial_data)
    applicant_data = validated_data.pop('applicant')
    applicant_user = ApplicantUser.objects.get(user=applicant_data)
    job_data = self.initial_data.pop('job')
    the_job = JobModel.objects.get(pk=job_data)
    new_application = ApplicationModel.objects.create(
            status=validated_data.pop('status'),
            job=the_job,
            applicant=applicant_user)
    return new_application

