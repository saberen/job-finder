from django.db import models
from user.models import ApplicantUser
from job.models import JobModel

class ApplicationModel(models.Model):
  applicant = models.ForeignKey(ApplicantUser, on_delete=models.CASCADE, null=True, blank=True, related_name='applications')
  job = models.ForeignKey(JobModel, on_delete=models.CASCADE, related_name='applications')
  status = models.CharField(
    max_length=2,
    choices=[
      ('W', 'wiating'),
      ('A', 'accept'),
      ('R', 'reject')
    ]
  )
