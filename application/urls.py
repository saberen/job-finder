from django.urls import path, include

from .views import ApplicationModelViewset
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register('', ApplicationModelViewset, base_name='newApplication')


urlpatterns = router.urls