
from rest_framework.permissions import BasePermission
from .models import ApplicationModel
from user.models import ApplicantUser


class IsJobOwner(BasePermission):
  def has_permission(self, request, view):
    return request.user.username == ApplicationModel.objects.get(pk=view.kwargs['pk']).job.owner.user.username


class IsApplicant(BasePermission):
  def has_permission(self, request, view):
    try:
      ApplicantUser.objects.get(user__username = request.user.username)
      return True
    except ApplicantUser.DoesNotExist:
      return False  