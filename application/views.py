from django.shortcuts import render

from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import AllowAny, IsAuthenticated, IsAdminUser

from .models import ApplicationModel
from .serializers import ApplicationModelSerializer
from .permissions import IsApplicant, IsJobOwner

from job.models import JobModel



class ApplicationModelViewset(viewsets.ModelViewSet):
  queryset = ApplicationModel.objects.all()
  serializer_class = ApplicationModelSerializer

  def perform_create(self, serializer):
    serializer.save(applicant=self.request.user, status='W')

  def get_permissions(self):
    print(self.action)
    if self.action == 'create':
      return [IsApplicant()]
    elif self.action == 'partial_update':
      return [IsJobOwner()]
    else:
      return [IsAdminUser()]