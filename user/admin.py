from django.contrib import admin
from .models import ApplicantUser, EmployerUser, Field

admin.site.register(ApplicantUser)
admin.site.register(EmployerUser)
admin.site.register(Field)
