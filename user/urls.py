from django.urls import path, include

from .views import ApplicantViewset, EmployerViewset
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register('applicants', ApplicantViewset, base_name='applicants')
router.register('empolyers', EmployerViewset, base_name='empolyers')

urlpatterns = router.urls