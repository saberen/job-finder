from rest_framework.permissions import BasePermission
from .models import ApplicantUser, EmployerUser

class IsOwnerApplicant(BasePermission):
  def has_permission(self, request, view):
    return request.user.username == ApplicantUser.objects.get(pk=view.kwargs['pk']).user.username

class IsOwnerEmployer(BasePermission):
  def has_permission(self, request, view):
    return request.user.username == EmployerUser.objects.get(pk=view.kwargs['pk']).user.username