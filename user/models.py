from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator
from django.contrib.auth.models import User


class Field(models.Model):
  name = models.CharField(max_length=20)

  def __str__(self):
    return self.name


class EmployerUser(models.Model):
  user = models.OneToOneField(
        User,
        on_delete=models.CASCADE,   
    )
  company_name = models.CharField(max_length=80)
  establish_year = models.IntegerField(
    validators=[
      MinValueValidator(1250),
      MaxValueValidator(1400)
    ]
  )
  address = models.CharField(max_length=200)
  telephone_number = models.CharField(max_length=12)
  fields = models.ManyToManyField(Field, blank=True, related_name='companies')

  def __str__(self):
    return self.company_name



class ApplicantUser(models.Model):
  user = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
    )
  age = models.IntegerField(
      validators=[
        MinValueValidator(15),
        MaxValueValidator(60)
      ]
    )
  gender = models.CharField(
      max_length=1,
      choices=[
        ('M', 'male'),
        ('F', 'female'),
      ]
    )
  resume = models.FileField(blank=True, null=True) # to do set upload_to
  skills = models.ManyToManyField(Field, blank=True, related_name='users')

  def __str__(self):
    return self.user.first_name + ' ' + self.user.last_name

  
