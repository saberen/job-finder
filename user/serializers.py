from rest_framework import serializers
from .models import ApplicantUser, EmployerUser, Field
from django.contrib.auth.models import User


class UserSerializer(serializers.ModelSerializer):
  class Meta:
    model = User
    fields = [
      'pk',
      'username',
      'password',
      'first_name',
      'last_name',
      'email'
    ] 

class FieldSerializer(serializers.ModelSerializer):
  class Meta:
    model = Field
    fields = [
      'name',
    ]



class ApplicantSerializer(serializers.ModelSerializer):
  user = UserSerializer()
  skills = FieldSerializer(many=True, read_only=False)
  class Meta:
    model = ApplicantUser
    fields = [
      'pk',
      'user',
      'age',
      'gender',
      'resume',
      'skills',
    ]
  
  def create(self, validated_data):
    print("applicant serializer")
    captured_skills = validated_data.pop('skills')
    user_data = validated_data.pop('user')
    new_user = UserSerializer.create(UserSerializer(), user_data)
    new_applicant = ApplicantUser(**validated_data)
    new_applicant.user = new_user
    new_applicant.save()
    for i in captured_skills:
      new_skill = Field.objects.get(name=i["name"])
      new_applicant.skills.add(new_skill)
    return new_applicant




class EmployerSerializer(serializers.ModelSerializer):
  user = UserSerializer()
  fields = FieldSerializer(many=True, read_only=False)
  class Meta:
    model = EmployerUser
    fields = [
      'pk',
      'user',
      'company_name',
      'telephone_number',
      'establish_year',
      'address',
      'fields',
    ]

  def create(self, validated_data):
    print("employer serializer")
    captured_fields = validated_data.pop('fields')
    user_data = validated_data.pop('user')
    new_user = UserSerializer.create(UserSerializer(), user_data)
    new_employer = EmployerUser(**validated_data)
    new_employer.user = new_user
    new_employer.save()
    for i in captured_fields:
      new_field = Field.objects.get(name=i["name"])
      new_employer.fields.add(new_field)
    return new_employer
