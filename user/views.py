from django.shortcuts import render
from django.contrib.auth.models import User

from rest_framework import viewsets
from rest_framework.permissions import AllowAny, IsAuthenticated, IsAdminUser
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from rest_framework import status

from .models import ApplicantUser, EmployerUser
from .serializers import ApplicantSerializer, EmployerSerializer
from .permissions import IsOwnerApplicant, IsOwnerEmployer



class ApplicantViewset(viewsets.ModelViewSet):
    queryset = ApplicantUser.objects.all()
    serializer_class = ApplicantSerializer
    permission_classes = (AllowAny, )

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        out_data = serializer.data
        print(out_data)
        out_data["token"] = Token.objects.create(user=User.objects.get(pk = out_data["user"]["pk"])).key
        print
        return Response(out_data, status=status.HTTP_201_CREATED, headers=headers)

    def get_permissions(self):
        print(self.action)
        if self.action == 'create':
            return [AllowAny()]
        elif self.action == 'partial_update':
            return [IsOwnerApplicant()]
        else:
            return [IsAdminUser()]


class EmployerViewset(viewsets.ModelViewSet):
    queryset = EmployerUser.objects.all()
    serializer_class = EmployerSerializer
    permission_classes = (AllowAny, )

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        out_data = serializer.data
        print(out_data)
        out_data["token"] = Token.objects.create(user=User.objects.get(pk = out_data["user"]["pk"])).key
        print
        return Response(out_data, status=status.HTTP_201_CREATED, headers=headers)

    def get_permissions(self):
        print(self.action)
        if self.action == 'create':
            return [AllowAny()]
        elif self.action == 'partial_update':
            return [IsOwnerEmployer()]
        else:
            return [IsAdminUser()]
