from rest_framework.permissions import BasePermission
from user.models import EmployerUser, ApplicantUser
from .models import JobModel

class IsOwner(BasePermission):
  def has_permission(self, request, view):
    return request.user.username == JobModel.objects.get(pk=view.kwargs['pk']).owner.user.username


class IsEmployer(BasePermission):
  def has_permission(self, request, view):
    try:
      EmployerUser.objects.get(user__username = request.user.username)
      return True
    except EmployerUser.DoesNotExist:
      return False  

class IsApplicant(BasePermission):
  def has_permission(self, request, view):
    print(request.uasr)
    try:
      ApplicantUser.objects.get(user__username = request.user.username)
      return True
    except ApplicantUser.DoesNotExist:
      return False  