from django.shortcuts import render
from django.core import serializers
from django.http import JsonResponse
from django.forms.models import model_to_dict

from rest_framework import viewsets, generics
from rest_framework.response import Response
from rest_framework.generics import UpdateAPIView, CreateAPIView
from rest_framework.permissions import AllowAny, IsAuthenticated, IsAdminUser
from rest_framework import status

from user.models import Field, ApplicantUser

from .models import JobModel
from .serializers import JobModelSerializer, FormSerializer
from .permissions import IsOwner, IsEmployer, IsApplicant
from .binarysearchtree import jobsBST

import json



class JobModelViewset(viewsets.ModelViewSet):
  queryset = JobModel.objects.all()
  serializer_class = JobModelSerializer
  
  def perform_create(self, serializer):
    new_job = serializer.save(owner=self.request.user)
    jobsBST.update(new_job)

  def get_permissions(self):
    if self.action == 'create':
      return [IsEmployer()]
    elif self.action == 'partial_update':
      return [IsOwner()]
    elif self.action == 'list':
      return [AllowAny()]
    else:
      return [IsAdminUser()]

  
class ShowJob(generics.GenericAPIView):
  permission_classes = (IsApplicant, )
  def get(self, request, *args, **kwargs):
    applicant = ApplicantUser.objects.get(user__username = request.user.username)
    skills = list(applicant.skills.all()) 
    found_jobs = []
    for i in skills:
      found_jobs += list(JobModel.objects.all().filter(fields__name = i.name))
    data = {}
    for i in found_jobs:
      dict_obj = model_to_dict( i )
      dict_obj.pop('picture')
      dict_obj.pop('fields')
      data[i.title] = dict_obj
    response = JsonResponse(status=status.HTTP_200_OK, data=data)
    return response


class FindJob(generics.GenericAPIView):
  serializer_class = FormSerializer
  permission_classes = (IsAuthenticated, )

  def post(self, request, *args, **kwargs):
    validated_data = request.data
    serializer = self.get_serializer(data=validated_data)
    serializer.is_valid(self) 
    all_jobs = list(JobModel.objects.all())
    desired_title = validated_data['title']
    found_jobs = []
    for i in all_jobs:
      if desired_title in i.title:
        found_jobs += [i]
    data = {}
    for i in found_jobs:
      dict_obj = model_to_dict( i )
      dict_obj.pop('picture')
      dict_obj.pop('fields')
      data[i.title] = dict_obj
    response = JsonResponse(status=status.HTTP_200_OK, data=data)
    return response


class FilterJob(generics.GenericAPIView):
  serializer_class = FormSerializer
  permission_classes = (IsAuthenticated, )

  # new version
  def post(self, request, *args, **kwargs):
    validated_data = request.data
    serializer = self.get_serializer(data=validated_data)
    serializer.is_valid(self) 
    all_jobs = list(JobModel.objects.all())
    min_salary = validated_data['min_salary']
    max_salary = validated_data['max_salary']
    found_jobs = jobsBST.filter(min_salary, max_salary)
    data = {}
    for i in found_jobs:
      dict_obj = model_to_dict( i )
      dict_obj.pop('picture')
      dict_obj.pop('fields')
      data[i.title] = dict_obj
    response = JsonResponse(status=status.HTTP_200_OK, data=data)
    return response

  # last version 
  # def post(self, request, *args, **kwargs):
  #   validated_data = request.data
  #   serializer = self.get_serializer(data=validated_data)
  #   serializer.is_valid(self) 
  #   all_jobs = list(JobModel.objects.all())
  #   min_salary = validated_data['min_salary']
  #   max_salary = validated_data['max_salary']
  #   found_jobs = []
  #   for i in all_jobs:
  #     job_salary = i.salary
  #     if min_salary < job_salary < max_salary:
  #       found_jobs += [i]
  #   data = {}
  #   for i in found_jobs:
  #     dict_obj = model_to_dict( i )
  #     dict_obj.pop('picture')
  #     dict_obj.pop('fields')
  #     data[i.title] = dict_obj
  #   response = JsonResponse(status=status.HTTP_200_OK, data=data)
  #   return response
  
  