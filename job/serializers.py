from django.contrib.auth.models import User

from rest_framework.authtoken.models import Token
from rest_framework import serializers

from user.models import ApplicantUser, EmployerUser, Field
from user.serializers import FieldSerializer

from .models import JobModel

class JobModelSerializer(serializers.ModelSerializer):
  fields = FieldSerializer(many=True, read_only=False)
  class Meta:
    model = JobModel
    fields = [
      'pk',
      'title',
      'expire_date',
      'picture',
      'fields',
      'salary',
      'monthly_hours',
      'owner',
    ] 


  def create(self, validated_data):
    print("job serializer")
    captured_feilds = validated_data.pop('fields')
    user_data = validated_data.pop('owner')
    owner = EmployerUser.objects.get(user=user_data)
    new_job = JobModel.objects.create(title=validated_data.pop('title'), 
            expire_date=validated_data.pop('expire_date'),
            picture=validated_data.pop('picture'), 
            salary=validated_data.pop('salary'),
            monthly_hours=validated_data.pop('monthly_hours'),
            owner=owner)
    for i in captured_feilds:
      new_field = Field.objects.get(name=i["name"])
      new_job.fields.add(new_field)
    return new_job


class FormSerializer(serializers.Serializer):
  title = serializers.CharField(required=False)
  min_salary = serializers.IntegerField(required=False)
  max_salary = serializers.IntegerField(required=False)