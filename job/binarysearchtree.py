from .models import JobModel

class Node():
  def __init__(self, val, obj):
    self.value = val
    self.object = obj
    self.left = None
    self.right = None

  def get(self):
    return self.value
    
  def add_child(self, new_child):
    if new_child.value < self.value and self.left is None:
      self.left = new_child
    elif new_child.value < self.value and self.left != None:
      self.left.add_child(new_child)
    elif new_child.value >= self.value and self.right is None:
      self.right = new_child
    elif new_child.value >= self.value and self.right != None:
      self.right.add_child(new_child)

  def filter(self, minimum, maximum):
    less = []
    more = []
    if self.value >= minimum and self.left != None:
      less = self.left.filter(minimum, maximum)
    if self.value < maximum and self.right != None:
      more = self.right.filter(minimum, maximum)
    if minimum <= self.value <= maximum:
      return less + [self.object] + more
    else:
      return less + more


class BST():
  
  def __init__(self):
    self.root = None
    jobs = list(JobModel.objects.all())
    for obj in jobs:
      if self.root is None:
        self.root = Node(obj.salary, obj)
      else:
        self.root.add_child(Node(obj.salary, obj))
  
  def update(self, new_obj):
    self.root.add_child(Node(new_obj.salary, new_obj))

  def filter(self, minimum, maximum):
    return self.root.filter(minimum, maximum)



jobsBST = BST()