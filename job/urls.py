from django.urls import path, include

from .views import JobModelViewset, ShowJob, FindJob, FilterJob
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register('', JobModelViewset, base_name='newjobs')

urlpatterns = [
  path('show', ShowJob.as_view(), name='desired-jobs'),
  path('search', FindJob.as_view(), name='find-jobs'),
  path('filter', FilterJob.as_view(), name='salary-filter')
]




urlpatterns += router.urls