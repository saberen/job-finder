from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator
from user.models import Field, EmployerUser, ApplicantUser
from django.contrib.auth.models import User


class JobModel(models.Model):
  title = models.CharField(max_length=80, unique=True)
  expire_date = models.DateField()
  picture = models.ImageField(blank=True, null=True)
  fields = models.ManyToManyField(Field, blank=True)
  salary = models.IntegerField()
  monthly_hours = models.IntegerField()
  owner = models.ForeignKey(EmployerUser, on_delete=models.CASCADE, null=True, blank=True)

  def __str__(self):
    return self.title
